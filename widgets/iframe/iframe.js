widget = {
  onData: function (el, data) {
    if (data.title) {
      $('h2', el).text(data.title);
    } else{
      $('h2', el).remove();
    }
    if (data.zoom && data.zoom>0){
      $('iframe', el).prop("width", 100/data.zoom+"%");
      $('iframe', el).prop("height", 100/data.zoom+"%");
      $('iframe', el).css("-webkit-transform", "scale("+data.zoom+")");
      $('iframe', el).css("-webkit-transform-origin", "0 0");
    }
    $(el).find('.content iframe').attr('src', data.url);
  }
};